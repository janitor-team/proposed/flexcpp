#include "scanner.ih"

void Scanner::maybeSwitchStream()
{
    bool isComment = false;

    string text = String::trim(matched().substr(sizeof("//include")));

    if (text[0] == '"' && *text.rbegin() == '"')    // "s around the name
        text = text.substr(1, text.length() - 2);   // rm the "s
    else                                            // blanks in the name, but
        isComment = text.find_first_of(" \t")       // no surrounding "s
                    != string::npos;

    if (not isComment)
        isComment = not fs::exists(text);           // is the file does not
                                                    // exist  //incl. is 
                                                    // considered comment
    if (isComment)
    {
        wmsg << '`' << matched() << "' considered comment" << endl;
        return;
    }

    setLineTags(text);
    pushStream(text);
}


