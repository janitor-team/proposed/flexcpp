
Character constants are surrounded by single quote characters. They match
single characters which, however, can be specified in various ways.
    itemization(
    it() The simplest form consists of just a single character: the pattern
        tt('a') matches the character tt(a), the pattern tt('.') matches the
        dot-character (tt(.) thus loses its meaning of `any character but
        the newline character');
    it() Standard escape characters (like tt('\n', '\f', '\b')) are converted
        to their (single character) ascii-values, matching those characters
        when they are encountered in the input. Therefore, of the following
        two rules the second is never matched (with flc() generating a
        corresponding warning, since both match the newline character):
       verb(
    '\n'    return 1;
    \n      return 2;
       )
    it() Octal numbers, starting with a backslash and consisting of three
        octal digits are converted to a number matching input characters of
        those numbers. E.g., tt('\101') is converted to 65, matching ascii
        character tt(A);
    it() Likewise, hexadecimal numbers, starting with tt(x) and followed by
        two hexadecimal digits are converted to a number matching input
        characters whose values equal those numbers. E.g., tt('\x41') is also
        matching ascii character tt(A);
    it() Other escaped single characters match those characters. E.g.,
        tt('\\') matches the single backslash, tt('\'') matches the single
        quote character. But also: tt('\F') matches the single tt(F)
        character, since no special escaped meaning is associated with tt(F).
    )
    
Considering the above, to match character (in this example: except for the
newline character) including its surrounding quotes a regular expression
consisting of an escaped quote character, followed by any character, followed
by a quote character can be used:
        verb(
    \'.'        // matches characters surrounded by quotes
        )
