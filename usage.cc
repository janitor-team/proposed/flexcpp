#include "main.ih"

    // Cf bisonc++'s usage.cc for an example of how to write usage().

namespace
{
    char const usageTop[] = R"_( [options] infile
Where:
  [options] - zero or more optional arguments, where [options] are:
   --baseclass-header=<filename> (-b):
           filename for the base class definition.
   --baseclass-skeleton=<path> (-B):
           location of the baseclass header skeleton.
   --case-insensitive:
           regexes disregard letter casing.
   --class-header=<filename> (-c):
           filename for the class definition.
   --class-name=<classname> (-c):
           class name of the generated scanner class.
   --class-skeleton=<path> (-C):
            location of the class header skeleton.
   --construction (-K):
           write information about the rules and DFA-construction on
           'infile'.output.
   --debug (-d):
           provides the generated lex function with debug statements
   --filenames=<basename> (-f):
           use 'basename' instead of 'Scanner' for generated files
   --force-class-header:
           overwrite an existing class header file.
   --force-implementation-header:
           overwrite an existing implementation header file.
   --help (-h):
           display this help-message.
   --implementation-header=<filename> (-i):
           filename for the implementation header.
   --implementation-skeleton=<path> (-I):
            location of the implementation header skeleton.
   --lex-function-name=<path>:
            name of the public lex-function.
   --lex-skeleton=<path> (-P):
            location of the lex function's skeleton.
   --lex-source=<filename> (-l):
            filename for the lex function's source.
   --matched-rules (-R):
            show the numbers of matched rules on stdout
            (implied by --debug)
   --max-depth=<maxDepth> (-m):
            set the max. scanner specification file inclusion depth.
   --namespace=<identifier> (-n):
            define the scanner and scanner base classes in the
            `namespace identifier'
   --no-baseclass-header:
            the baseclass header is not rewritten.
   --no-lex-source:
            the source containing lex() is not rewritten.
   --no-lines:
            do not add #line preprocessor directives to the lex function.
   --own-parser (-P):
           flexc++ writes the actions of its own parser to the standard
           output stream.
   --own-tokens (-T):
           flexc++ displays the tokens and their corresponding
           matched text it received from its own lexcial scanner.
   --print-tokens (-t):
           the print() member of the generated parser class displays
           the tokens and their corresponding matched text.
   --regex-calls:
           show function call order when parsing regexes
   --show-filenames (-F):
            display the names of generated files.
   --skeleton-directory=<path> (-S):
            location of the skeleton directory.
   --target-directory=<path>:
            target directory for generated files.
   --usage (-h):
           display this help-message.
   --verbose (-V):
           provide additional info on stdout
   --version (-v):
           display )_";

    char const usageBottom[] = R"_('s version and terminate.

  `infile' - name of the file containing the scanner specifications

)_";


} // namespace

void usage(string const &program_name)
{
    cout << 
        "\n" <<
        program_name << " by Frank B. Brokken (f.b.brokken@rug.nl)\n"
        "                 and Jean-Paul van Oosten (j.p.van.oosten@rug.nl)\n"
        "\n"
        "Lexical Scanner Generator V "  << version << "\n"
        "Copyright (c) GPL " << year << ". NO WARRANTY.\n"
        "\n"
        "Usage: " << program_name << usageTop << program_name << usageBottom;
}



