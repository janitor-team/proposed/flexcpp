#include "pattern.ih"

bool Pattern::canBeEmpty(States const &states) const
{
    set<size_t> indices;

    return 
        (fixedLength() && length() == 0)
        or
        empty(indices, states, d_pair.first);
}
